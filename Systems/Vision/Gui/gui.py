import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from PyQt5 import QtNetwork as qtn
from PyQt5.QtCore import (QThread, pyqtSignal, pyqtSlot)
from PyQt5.QtGui import (QPixmap, QImage)
from Display import Display


#class Udp(qtc.QObject):
#    def __init__(self, addr="127.0.0.1", port=1234):
#        super().__init__()
#        self.socket = qtn.QUdpSocket()
#        self.socket.bind(qtn.QHostAddress.LocalHost, port)
#        self.socket.readyRead.connect(self.getDatagrams)

#    def getDatagrams(self):
#        datagram = self.socket.receiveDatagram()
#        return datagram


class VideoThread(QThread):
    updateFront = pyqtSignal(QImage)
    def __init__(self, parent=None):
        QThread.__init__(self, parent=parent)
        self._isRunning = True
        #self.s = Udp()
        #self._isbufferDumped = 0
    def run(self):
        global feedOne
        if self._isRunning:
            feedOne = Display(0)
        while self._isRunning:
            ret, frontView = feedOne.grabFrame()
            front = QImage(frontView.data, frontView.shape[1],frontView.shape[0], QImage.Format_RGB888).rgbSwapped()
            self.updateFront.emit(front)
        if not self._isRunning:
            feedOne.close()


    def close(self):
        self._isRunning = False


class MainWindow(qtw.QWidget):
    def __init__(self):
        #Constructr for child class of MainWindow
        super().__init__() #inheritance
        self.title = "45C Robotics 2021"
        self.initUI()
        window = qtw.QWidget(cursor=qtc.Qt.ArrowCursor)
        #button = qtw.QPushButton("Push Me", self)

        #combobox = qtw.QComboBox(self)
        #combobox.addItem("Reset", qtw.QPushButton) #creates list of buttons
        self.show()

    @pyqtSlot(QImage)
    def setFront(self, image):
        self.frontHandler.setPixmap(QPixmap.fromImage(image))

    def initUI(self):
        self.setWindowTitle(self.title)
        self.resize(1920,1080)

        #Crate label for images to be displayed
        self.frontHandler = qtw.QLabel(self)
        self.frontHandler.move(150,0)
        self.frontHandler.resize(925,500)

        #create Video Thread
        self.th = VideoThread(self)
        self.th.updateFront.connect(self.setFront)
        self.th.start()



if __name__ == '__main__':
    app = qtw.QApplication(sys.argv) #creating the QApplication object
    #at the global scope is not required but ensures that all Qt objects gets
    #properly closed when the application is closed
    mW = MainWindow()
    sys.exit(app.exec())
    #app.exce() is inside the call to sys.exit()
    #to pass the exit code of app.exec() to be passed to sys.exit()if the Qt instance crashes
