import cv2
import gi


cap= cv2.VideoCapture('udpsrc port=5001 ! application/x-rtp,media=video,payload=26,clock-rate=90000,encoding-name=JPEG! rtpjpegdepay ! jpegdec ! videoconvert ! appsink', cv2.CAP_GSTREAMER)


if not cap.isOpened():
    print("Video is Ded")
    exit(0)

while True:
    ret,frame = cap.read()
    if not ret:
        print("rip frame")
        break
    cv2.imshow("Frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
