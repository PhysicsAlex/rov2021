import cv2
import gi


cap= cv2.VideoCapture('udpsrc port=5000 ! application/x-rtp,media=video,payload=26,clock-rate=90000,encoding-name=JPEG,framerate=30/1 ! rtpjpegdepay ! jpegdec ! videoconvert ! appsink', cv2.CAP_GSTREAMER)


if not cap.isOpened():
    print("Video is Ded")
    exit(0)

while True:
    ret,frame = cap.read()
    if not ret:
        print("rip frame")
        break
    cv2.imshow("Receiver", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
