from __future__ import division
import cv2
import numpy as np
import socket
import struct

class Display():
    def __init__(self, num=0):
        self.num = num
        self.cap = cap= cv2.VideoCapture('udpsrc port=5000 ! application/x-rtp,media=video,payload=26,clock-rate=90000,encoding-name=JPEG,framerate=30/1 ! rtpjpegdepay ! jpegdec ! videoconvert ! appsink', cv2.CAP_GSTREAMER)

    def dumpBuffer(self):
        self.seg, self.addr = self.s.recvfrom(self.MAX_DGRAM)
        print(self.seg)
        return struct.unpack("B", self.seg[0:1])[0]


    def grabFrame(self):
        #dat = b''
        #self.seg, self.addr = self.s.recvfrom(self.MAX_DGRAM)
        #print(self.seg)
        #if struct.unpack("B", self.seg[0:1])[0] > 1:
        #    dat += self.seg[1:]
        #else:
        #    dat += self.seg[1:]
            #print(dat)
        #    img = cv2.imdecode(np.fromstring(dat, dtype=np.uint8), 1)
            #dat = b''
        #    return (True,img)

        #img = cv2.imread("reset.png")
        #return(None, None)
        ret, image = self.cap.read()
        if image is not None:
            return (ret, image)
        return (ret,None)

    def close(self):
        self.cap.release()

if __name__ == "__main__":
    d = Display()
    while True:
        ret, frame = d.grabFrame()
        cv2.imshow("frame", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        dat = b''
    d.close()
    cv2.destroyAllWindows()
