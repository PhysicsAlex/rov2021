#include <QTime>
#include <QtCore>
#include <QDebug>
#include "capture_thread.h"

CaptureThread::CaptureThread(int camera, QMutex *lock):running(false), cameraID(camera), videoPath(""), data_lock(lock){

}

CaptureThread::CaptureThread(QString videoPath, QMutex *lock):running(false), cameraID(-1), videoPath(videoPath), data_lock(lock){

}

CaptureThread::~CaptureThread(){

}

void CaptureThread::run(){
    running = true;
    cv::VideoCapture cap(cameraID);
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
    //initialize sockets
    cv::Mat tmp_frame;
    while(running){
        cap >> tmp_frame;
        if(tmp_frame.empty()){
            break;
        }
        cvtColor(tmp_frame, tmp_frame, cv::COLOR_BGR2RGB);
        data_lock->lock();
        frame = tmp_frame;
        data_lock->unlock();
        emit frameCaptured(&frame);
    }
    cap.release();
    running = false;

}
