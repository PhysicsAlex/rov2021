#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <QKeyEvent>
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    resetMenu(nullptr),
    capturer(nullptr)
{
    ui->setupUi(this);
    initUI();
    data_lock = new QMutex();
}

void MainWindow::initUI(){
    resetMenu = menuBar()->addMenu("&Reset");
    //QGridLayout *main_layout = new QGridLayout();
    imageScene = new QGraphicsScene(this);
    ui->graphicsView->setScene(imageScene);
    //imageScene = new QGraphicsScene(this);
    imageView = new QGraphicsView(imageScene);
    //main_layout->addWidget(imageView, 0, 0, 12, 1);
    createActions();

}
void MainWindow::createActions(){
    openCameraAction = new QAction("&Open Camera", this);
    resetMenu->addAction(openCameraAction);
    connect(openCameraAction, SIGNAL(triggered(bool)), this, SLOT(openCamera()));
}
void MainWindow::openCamera(){
    if(capturer != nullptr){
        capturer->setRunning(false);
        disconnect(capturer, &CaptureThread::frameCaptured, this, &MainWindow::updateFrame);
        connect(capturer, &CaptureThread::finished, capturer, &CaptureThread::deleteLater);
    }
    int cameraID = 0;
    capturer = new CaptureThread(cameraID, data_lock);
    connect(capturer, &CaptureThread::frameCaptured, this, &MainWindow::updateFrame);
    capturer->start();

}
void MainWindow::updateFrame(cv::Mat *mat){
    data_lock->lock();
    currentFrame = *mat;
    data_lock->unlock();
    QImage frame(currentFrame.data, currentFrame.cols, currentFrame.rows, currentFrame.step, QImage::Format_RGB888);
    QPixmap image = QPixmap::fromImage(frame);
    imageScene->clear();
    imageView->resetMatrix();
    imageScene->addPixmap(image);
    imageScene->update();
    imageView->setSceneRect(image.rect());
}
MainWindow::~MainWindow()
{
    delete ui;
}
