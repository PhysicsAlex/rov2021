#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMenuBar>
#include <QAction>
#include <capture_thread.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initUI();
    void createActions();

private slots:
    void openCamera();
    void updateFrame(cv::Mat*);

private:
    Ui::MainWindow *ui;
    QMenu *resetMenu;
    QAction *openCameraAction;
    QGraphicsScene *imageScene;
    QGraphicsView *imageView;
    cv::Mat currentFrame;
    QMutex *data_lock;
    CaptureThread *capturer;
};

#endif // MAINWINDOW_H
