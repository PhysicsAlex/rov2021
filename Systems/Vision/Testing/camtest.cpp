#include <opencv2/opencv.hpp>
#include <iostream>
using namespace cv;

int main() {
  VideoCapture cap(1);
  cap.set(CAP_PROP_FRAME_WIDTH, 640);
  cap.set(CAP_PROP_FRAME_HEIGHT, 480);

  if(!cap.isOpened()) {
    std::cout << "cannot open camera";
  }

  while (cap.isOpened()) {
    Mat cameraFrame;
    cap.read(cameraFrame);
    imshow("cam", cameraFrame);
    if(waitKey(30) >= 0)
      break;
    }

  return 0;
}
